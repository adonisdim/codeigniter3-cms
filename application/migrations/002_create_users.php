<?php
class Migration_Create_users extends CI_Migration {
    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '128',
            ),
            'password' => array(
                'type' => 'VARCHAR',
                'constraint' => '128',
            ),
            'email' => array(
                'type' => 'VARCHAR',
		'constraint' => '128',
            ),
            'status' => array(
		'type' => 'TINYINT',
		'constraint' => 11,
                'unsigned' => TRUE
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('ci_users');
    }
    public function down()
    {
	$this->dbforge->drop_table('ci_users');
    }
}