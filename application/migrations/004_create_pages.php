<?php
class Migration_Create_pages extends CI_Migration {

	public function up()
	{
		$this->dbforge->add_field(array(
			'id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'auto_increment' => TRUE
			),
			'parent_id' => array(
				'type' => 'INT',
				'constraint' => 11,
				'unsigned' => TRUE,
				'default' => 0
			),
			'meta_title' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'meta_keywords' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'meta_description' => array(
				'type' => 'TEXT',
			),
			'title' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'slug' => array(
				'type' => 'VARCHAR',
				'constraint' => '100',
			),
			'body' => array(
				'type' => 'TEXT',
			),
                        'template' => array(
				'type' => 'VARCHAR',
				'constraint' => '25',
			),
			'order' => array(
				'type' => 'INT',
				'constraint' => '11',
			),
			'published' => array(
				'type' => 'BOOLEAN',
				'default' => TRUE,
			)

		));
		$this->dbforge->add_key('id', TRUE);
		$this->dbforge->create_table('ci_pages');
	}
	public function down()
	{
		$this->dbforge->drop_table('ci_pages');
	}
}