<?php
defined("BASEPATH") or exit("No direct script access allowed");

Class Migration extends Backend_Controller{
    public function __construct() {
        parent::__construct();
    }
    public function index($version) {
        /*run migration*/        
        $this->load->library('migration');
        if(!$this->migration->version($version)){
            show_error($this->migration->error_string());
        }else{
            echo 'Database schema updated succesfully';
        } 
    }
}
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

