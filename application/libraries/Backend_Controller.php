<?php
class Backend_Controller extends MY_Controller
{ 
    public function __construct() {
        parent::__construct();
        $this->data['meta_title']= "Admin ";
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
}

